// Validation function
$(function() {
    // Setup form validation on the #myForm element
    $('#uploadForm').validate({

        // Specify the validation rules
        rules: {

            nomImatge: {
                required: true
            },

            urlImatge: {
                required: true,
                url: true
            }
        },

        messages: {

            nomImatge: {
                required: "Has d'introduïr un nom per la imatge"
            },

            urlImatge: {
                required: "Has d'introduïr una URL per la imatge",
                url: "Has d'introduïr una URL vàlida per la imatge"
            }
        },

        submitHandler: function(form) {
            form.submit();
        }
    });
});
