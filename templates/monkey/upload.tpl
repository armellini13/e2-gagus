{$modules.head}

<div class="block">
    <div id="inner-block" align="center">
        <h2 id="upload">Upload</h2>

    {   if $allOk}
            <p>Gràcies per la teva participació!<p>
            <br><br>
            <a href= {$url.global}/upload>Tornar</a>
    {   else}
            <form id="uploadForm" action="" method="POST">
                Nom de la imatge: <input type="text" id="nomImatge" name="nomImatge">
                <br>
                URL de la imatge: <input type="text" id="urlImatge" name="urlImatge">
                <br>
                <input type="submit" value="Submit">
            </form>
    {   /if}

        <br><br>
    </div>
</div>

<div class="clear"></div>

{$modules.footer}