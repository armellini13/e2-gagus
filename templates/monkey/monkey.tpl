{$modules.head}

<!-- Això és un comentari HTML -->
{* Això és un comentari en Smarty *}

<div class="block">
    <div id="inner-block" align="center">
        <h2 id="monkeyNumber">{$monkeyName}</h2>
        <img src="{$URL}">
        <br><br>
    {   if !$empty}
        <a href="{$url.global}/monkey/{$anterior}">
                Anterior
        </a>

        <a href="{$url.global}/monkey/{$seguent}">
            Següent
        </a>
    {   else}
        <p>Ho sentim però no tenim cap fotografia a la galeria. Pots pujar fotos a l'apartat d'Upload!</p>
    {   /if}
        <br><br>
    </div>
</div>

<div class="clear"></div>
{$modules.footer}