<?php

/*
 * Monkey Controller:
 * S'encarrega de mostrar la pàgina amb el mico corresponent.
 */
class MonkeyUploadController extends Controller
{
    protected $view = 'monkey/upload.tpl';
    private $uploadModel;

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {
        $error = false;
        $uploadModel = $this->getClass('MonkeyUploadModel');

        // Agafem els paràmetres
        $info = $this->getParams();

        if(isset($info["url_arguments"]) &&
            !(sizeof($info["url_arguments"]) == 1 && $info["url_arguments"][0] == "")) {

            $error = true;

        }

        // Si hi ha error
        if($error)
            $this->setLayout('error/error404.tpl');

        else {

            $this->updateForm();
            $this->setLayout($this->view);

        }

    }

    protected function updateForm()
    {
        // Per capturar una variable enviada mitjan�ant un formulari HTML pels m�todes GET/POST, has de fer servir
        // la class Filter de la seg�ent manera.
        $nom = Filter::getString('nomImatge');
        $url = Filter::getURL('urlImatge');

        // Si $nom �s un String, entrar� a l'if. Altrament, $nom ser� fals.
        if($nom && $url) {
            $this->assign('allOk',true);
            $uploadModel = $this->getClass('MonkeyUploadModel');
            $uploadModel->setMonkey($nom, $url);

        }
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}