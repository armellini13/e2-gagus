<?php

/*
 * Monkey Controller:
 * S'encarrega de mostrar la pàgina amb el mico corresponent.
 */
class MonkeyMonkeyController extends Controller
{
    protected $view = 'monkey/monkey.tpl';
    private $img;

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {
        // Flags de control
        $error = false;
        $noMonkey = false;

        // Agafem els paràmetres
        $info = $this->getParams();

        // Agafem la mida de la galeria
        $monkeyModel = $this->getClass('MonkeyMonkeyModel');
        $gallerySize = $monkeyModel->getSizeGallery()[0]['count(*)'];

        if(isset($info["url_arguments"])) {

            $monkeyNumber = $info["url_arguments"][0];

            // Si hi han més d'un paràmetre hi ha error 404 sino seguim
            if(sizeof($info["url_arguments"]) == 1) {
                if($info["url_arguments"][0] == "") {
                    $noMonkey = true;
                }
                else {
                    // Si el mico no està a la galeria hi ha error 404 sino mostrem el mico
                    if(!($monkeyNumber >= 0 && $monkeyNumber <= $gallerySize - 1))
                        $error = true;
                    else
                        if($monkeyNumber == 0)
                            $noMonkey = true;
                }
            }
            else {
                if(!(sizeof($info["url_arguments"]) == 2 && $info["url_arguments"][1] == ""))
                    $error = true;
            }

        }
        else
            $noMonkey = true;

        // Mirem si hi ha error
        if($error)
            $this->setLayout('error/error404.tpl');

        else {

            if($gallerySize > 0){


                $info = $monkeyModel->getMeMonkey();

                if($noMonkey || !$monkeyNumber) {

                    $monkeyNumber = 0;
                    $this->assign('anterior', $gallerySize - 1);
                    if($gallerySize == 1)
                        $this->assign('seguent', $gallerySize - 1);
                    else
                        $this->assign('seguent',1);
                }

                else {

                    $this->assign('anterior', $monkeyNumber - 1);
                    if($gallerySize -1  == $monkeyNumber)
                        $this->assign('seguent', 0);
                    else
                        $this->assign('seguent', $monkeyNumber + 1);
                }

                $this->assign('monkeyName',$info[$monkeyNumber]['name']);
                $this->assign('URL',$info[$monkeyNumber]['URL']);
                $this->assign('empty',false);
            }

            else
                $this->assign('empty',true);

            $this->setLayout($this->view);
        }

    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}