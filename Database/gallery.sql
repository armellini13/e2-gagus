-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-03-2014 a las 21:44:51
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gallery`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marmot`
--

CREATE TABLE IF NOT EXISTS `marmot` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marmot`
--

INSERT INTO `marmot` (`ID`, `Name`, `URL`) VALUES
('0', 'Mar 1', 'http://tolweb.org/tree/ToLimages/2536110619_5b5530083b.250a.jpg'),
('1', 'Mar 2', 'http://4.bp.blogspot.com/-7Hi3CfkTXXM/TWiwgWEcTVI/AAAAAAAADDc/GTQbzibBhxY/s1600/marmota%201.jpg'),
('3', 'Mar 4', 'http://www.que.es/archivos/201302/dia_marmota_n-672xXx80.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monkey`
--

CREATE TABLE IF NOT EXISTS `monkey` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `monkey`
--

INSERT INTO `monkey` (`ID`, `Name`, `URL`) VALUES
('0', 'Monkey', 'http://img3.wikia.nocookie.net/__cb20130606164012/animalcrossing/images/3/30/Monkey.jpg'),
('1', 'Monkey 2', 'http://www.hdpaperwall.com/wp-content/uploads/2014/02/sangeh-monkey-forest-10.jpg'),
('2', 'Monkey 3', 'http://sciencelakes.com/data_images/out/1/8766215-monkey.jpg'),
('3//images.', 'ads', 'http://images.nationalgeographic.com/wpf/media-live/photos/000/007/cache/spider-monkey_719_600x450.jpg'),
('4//impress', 'asdf', 'http://impressivemagazine.com/wp-content/uploads/2013/07/interesting_facts_about_monkeys_cute_monkey.jpg'),
('5//blog.li', 'monkey7', 'http://blog.lib.umn.edu/walsh414/chelsaewalsh/capuchin-monkey-wallpaper--1080x960.jpg'),
('6http:', 'asdsd', 'http://www.pageresource.com/wallpapers/wallpaper/animals-sweet-beautiful-high-definition-monkey-picture-free_313298.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `platypus`
--

CREATE TABLE IF NOT EXISTS `platypus` (
  `ID` varchar(10) NOT NULL DEFAULT '0',
  `Name` varchar(30) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `platypus`
--

INSERT INTO `platypus` (`ID`, `Name`, `URL`) VALUES
('0', 'Plat 1', 'http://gifts.worldwildlife.org/gift-center/Images/large-species-photo/large-Duck-billed-Platypus-photo.jpg'),
('1', 'Plat 2', 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSSgg4hROzPXGo3rzgWe-3jEKdALUevzd9MIj-TkUMP2vqO54M8iQ'),
('2', 'Plat 3', 'http://www.australiantraveller.com/wp-content/uploads/2011/03/100-Things-To-Do-Before-You-Die-24-Platypus-Featured-Image.jpg'),
('3', 'Plat 4', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGBZchPj_snXHUCzG2aN2zWcGGO0KjmPIklSKqmenPDO1rAfLo');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
