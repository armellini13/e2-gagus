<?php

/*
 * Model que s'encarrega de retornar les fotos dels micos
 * que es pujan a la pàgina web.
 */
class MonkeyMonkeyModel extends Model
{

    // Genera un ID per la foto
    public function getMeMonkey() {

        $sql = <<<QUERY
SELECT
    name, URL
FROM
    monkey
QUERY;

        return $this->getAll($sql);

    }

    public function getSizeGallery() {
        $sql =  <<<QUERY
SELECT
    count(*)
FROM
    monkey;
QUERY;

        return $this->getAll($sql);
    }
}