<?php

/*
 * Model que s'encarrega de guardar les fotos dels micos
 * que es pujan a la pàgina web.
 */
class MonkeyUploadModel extends Model
{

    // Genera un ID per la foto
    private function getMeAnID($url) {

        $sql = <<<QUERY
SELECT
    ID
FROM
    monkey;
QUERY;

        return count($this->getAll($sql)).substr($url,0,5);

    }

    // Guarda el mico dins de la DB
    public function setMonkey($nom, $url) {

        $id = $this->getMeAnID($url);

        $sql = <<<QUERY
INSERT INTO
    monkey
VALUES
    ( ?, ? , ? );
QUERY;

        $this->execute($sql,array($id, $nom, $url));

    }

}